---
title: "June"
date: 2023-07-01T14:45:37+01:00
---

# Arch Linux in June 2023

## packaging

We have now enabled all packagers to have default access to the multilib and
unstable desktop (GNOME/KDE) repositories. This decision removes artificial
gates and simplifies the process for packagers to contribute to different
aspects of the distro's packaging. By granting wider access by default, we
encourage easier participation and collaboration within our distro packaging
team.

## devtools

We released version `v1.0.3` of devtools which focused on bug fixes. This
release addresses the issue of broken repos for new packages by ensuring that
the actual PKGBUILD is always version controlled. Additionally, we implemented
the functionality to skip unofficial architectures, providing better support
for building AUR packages with `pkgctl`.

## arch-boxes

Starting with the `v20230601` release, arch-boxes images now include x64 UEFI
support in addition to legacy BIOS. The GRUB boot loader is used for both UEFI
and BIOS systems, ensuring a consistent implementation across different
hardware configurations. This is a great enhancement that allows using
arch-boxes images on UEFI-based systems.

## ArchISO

In the `archlinux-2023.06.01-x86_64.iso` release, the mDNS responder of
systemd-resolved is now enabled by default. This improvement enhances the user
experience by simplifying network connections during the installation process.

This enables convenient connectivity to the live environment via SSH from the
local network without the need to know the IP address. Users can simply use the
mDNS hostname `archiso.local`. The ArchWiki article ["Install Arch Linux via
SSH"][0] has been updated to include a helpful tip with the appropriate
command.

## container images

Arch Linux's container images are now published to multiple registries. In
addition to DockerHub, the images are also pushed to [Quay.io][1] and [GitHub
Container registry][2]. This wider distribution ensures availability of the
image across multiple platforms and helps mitigate the risk of a single point
of failure. Users can now access the Arch Linux container images from their
preferred registry, offering greater flexibility and redundancy.

## Specification effort for .BUILDINFO files

With the [alpm-buildinfo][3] project, a Rust-based specification effort for the
`.BUILDINFO` files found in ALPM packages has been started. The implementation
for version 1 of the file is currently in review.

[0]: https://wiki.archlinux.org/title/Install_Arch_Linux_via_SSH#On_the_local_machine
[1]: https://quay.io/repository/archlinux/archlinux
[2]: https://github.com/archlinux/archlinux-docker/pkgs/container/archlinux
[3]: https://gitlab.archlinux.org/archlinux/alpm/alpm-buildinfo
