all: check build

build:
	hugo

check:
	mdl content

clean:
	rm -rf public resources
